import { Profiler, useEffect, useState } from "react";
import "./App.css";

function App() {
  const [state, setState] = useState(0);
  const [loaded, setStatus] = useState(false);
  const [hitServer, setHitServer] = useState(false);

  const server = () => {
    const res = new Promise((resolve) => {
      setTimeout(() => {
        setState(state + 10);
        resolve("resolve");
      }, 3000);
    });

    return res;
  };

  const timeoutAfter = (seconds) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        reject(new Error("request timed out"));
      }, seconds * 1000);
    });
  };

  const collectData = async () => {
    const start = performance.now();
    try {
      // REQUEST WITH TIMEOUT
      // promise race dapat digunakan untuk membuat timeout
      // pada sebuah promise yang berjalan terlalu lama sehingga dapat dibatalkan lebih cepat
      // const res = await Promise.race([timeoutAfter(2), server()]);

      const res = await server();
      console.log(res, " : BERHASIL");
    } catch (error) {
      console.error(error.message);
    }
    setHitServer(true);
    const end = performance.now();
    console.log(end - start, "====    SERVER   ====");
  };

  // https://reactjs.org/docs/profiler.html
  const logTimes = (
    id, // the "id" prop of the Profiler tree that has just committed
    phase, // either "mount" (if the tree just mounted) or "update" (if it re-rendered)
    actualDuration, // time spent rendering the committed update
    baseDuration, // estimated time to render the entire subtree without memoization
    startTime, // when React began rendering this update
    commitTime // when React committed this update
  ) => {
    if (phase === "mount" || (loaded && hitServer)) {
      console.group(`${id} ${phase} phase:`);
      console.table({
        actualDuration: Number(actualDuration).toFixed(2) + " ms",
        baseDuration: Number(baseDuration).toFixed(2) + " ms",
        startTime: Number(startTime).toFixed(2) + " ms",
        commitTime: Number(commitTime).toFixed(2) + " ms",
      });
      console.groupEnd();

      if (hitServer) setHitServer(null);
    }
  };

  // This will run one time after the component mounts
  useEffect(() => {
    collectData();

    // callback function to call when event triggers
    const onPageLoad = () => {
      // https://stackoverflow.com/questions/7606972/measuring-site-load-times-via-performance-api
      // https://developer.mozilla.org/en-US/docs/Web/API/Navigation_timing_API
      setTimeout(() => {
        const pageLoadTime =
          performance.timing.loadEventEnd - performance.timing.navigationStart;
        console.log("page loaded");
        console.log(pageLoadTime, " ====    PAGE   ====");
      }, 0);
    };

    // Check if the page has already loaded
    // pengecekan ini dikarenakan fakta bahwa component mounts pada halaman
    // tidak berarti bahwa halaman tersebut dimuat sepenuhnya
    if (document.readyState === "complete") {
      onPageLoad();
      setStatus(document.readyState === "complete");
    } else {
      document.onreadystatechange = function () {
        if (document.readyState === "complete") {
          onPageLoad();
          setStatus(document.readyState === "complete");
        }
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Profiler id="App" onRender={logTimes}>
      {/* {loaded && hitServer
        ? console.log("loading page & hit server DONE")
        : hitServer !== null
        ? console.log("PROSES loading page && hit server")
        : null} */}
      <div className="App">
        <header className="App-header">
          <button
            onClick={() => {
              setState(state + 10);
            }}
          >
            CLICK
          </button>
        </header>
      </div>
    </Profiler>
  );
}

export default App;

// syntax: document.readyState
// return :
// - uninitialized - Has not started loading
// - loading - Is loading
// - loaded - Has been loaded
// - interactive - Has loaded enough to interact with
// - complete - Fully loaded

// id, // the "id" prop of the Profiler tree that has just committed
// prop "id" dari pohon Profiler yang baru saja dikomit

// phase, // either "mount" (if the tree just mounted) or "update" (if it re-rendered)
// baik "mount" (jika pohon baru saja dipasang) atau "update" (jika dirender ulang)

// actualDuration, // time spent rendering the committed update
// waktu yang dihabiskan untuk merender pembaruan yang dilakukan

// baseDuration, // estimated time to render the entire subtree without memoization
// perkiraan waktu untuk merender seluruh subpohon tanpa memoisasi

// startTime, // when React began rendering this update
// ketika React mulai merender pembaruan ini

// commitTime // when React committed this update
// ketika React melakukan pembaruan ini
